# interlingual homophonic homographs

import pandas as pd

# input
hyzd = pd.read_csv('hyzd', sep='\t')
yyzd = pd.read_csv('yyzd', sep='\t')
ss = pd.read_csv('ss', sep='\t')

# drop useless example characters
ss = ss.drop(columns=['char_hy', 'char_yy'])

# construct scss
scss_l = []
for hyzd_i in range(len(hyzd)):
    ss_i_l = ss.index[ss.pinyin == hyzd.pinyin[hyzd_i]]
    if len(ss_i_l):
        ss_i = ss_i_l[0]
        yyzd_i_l = yyzd.index[yyzd.char == hyzd.char[hyzd_i]]
        for yyzd_i in yyzd_i_l:
            if ss.jyutping[ss_i] == yyzd.jyutping[yyzd_i]:
                scss_l.append((hyzd.char[hyzd_i], ss.ipa[ss_i]))
                break
scss = pd.DataFrame(scss_l, columns=['char', 'ipa'])

# sort
scss = scss.sort_values(by='ipa') # not sorted by index
print(scss)

# output
scss.to_csv('scss', sep='\t', index=False)
