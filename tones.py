# tones sankey diagram between hy and yy

import pandas as pd
from textwrap import wrap

# input
hyyyzd = pd.read_csv('hyyyzd', sep='\t')

# remove light tone characters
hyyyzd = hyyyzd[~hyyyzd.pinyin.str.endswith('5')]

# remove entering tone characters
hyyyzd = hyyyzd[~hyyyzd.jyutping.str.contains('[ptk].$')]

sankey_link_sources = []
sankey_link_targets = []
for i in range(4):
    for j in range(4, 10):
        sankey_link_sources.append(i)
        sankey_link_targets.append(j)

groupby_tones = hyyyzd.groupby([hyyyzd.pinyin.str[-1], hyyyzd.jyutping.str[-1]])
sankey_link_values = list(groupby_tones.size())
sankey_link_labels = ['<br>'.join(wrap(s, 35)) for s in groupby_tones.sum().char]


import plotly.graph_objects as go

fig = go.Figure(data=[go.Sankey(
    orientation = 'v',
    valueformat = '.0f',
    node = dict(
        pad = 25,
        thickness = 25,
        line = dict(width=0),
        label = ['p.y. 1', 'p.y. 2', 'p.y. 3', 'p.y. 4',
                 'j.p. 1', 'j.p. 2', 'j.p. 3', 'j.p. 4', 'j.p. 5', 'j.p. 6']
    ),
    link = dict(
        source = sankey_link_sources,
        target = sankey_link_targets,
        value  = sankey_link_values,
        label  = sankey_link_labels
    )
)])
fig.update_layout(
    title_text = 'Tones',
    title_x = 0.5,
    font_size = 20
)

fig.show(config=dict(displaylogo=False))
