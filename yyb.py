import pandas as pd

# get necessary data from input
yyb = pd.read_csv('yyb_orig', sep='\t', usecols=[5, 6], header=None)[[6, 5]]
# print(yyb)
yyb.columns = ['jyutping', 'ipa']
# print(yyb)

# modify
# 'lengthen' tones with no change in pitch to match hyb notation. maybe inaccurate for checked tones with short vowels
yyb.ipa = yyb.apply(lambda x: x.ipa + (x.ipa[-1] if int(x.jyutping[-1]) in (1, 3, 6) else ''), axis=1)
# remove lowel length sign to match hyb notation. short vowels also differ in quality so this does not introduce ambiguity
yyb.ipa = yyb.ipa.str.replace('ː', '')
# change ipa semivowels to vowels to match hyb notation
yyb.ipa = yyb.ipa.str.replace('wu', 'u')
yyb.ipa = yyb.ipa.str.replace('w', 'u')
yyb.ipa = yyb.ipa.str.replace('jy', 'y')
yyb.ipa = yyb.ipa.str.replace('ji', 'i')
yyb.ipa = yyb.ipa.str.replace('j', 'i')
# print(yyb)

# sort
yyb = yyb.sort_values(by='jyutping') # not sorted by index
print(yyb)

# output
yyb.to_csv('yyb', sep='\t', index=False)
