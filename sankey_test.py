import plotly.graph_objects as go

fig = go.Figure(data=[go.Sankey(
    # orientation = 'v',
    valueformat = '.0f',
    valuesuffix = '個',
    node = dict(
        pad = 50,
        thickness = 25,
        line = dict(width=0),
        label = ['A1', 'A2', 'A3', 'B1', 'B2', 'B3'],
        color = ['red', 'orange', 'yellow', 'green', 'blue', 'purple'],
        customdata = ['甲一', '甲二', '甲三', '乙一', '乙二', '乙三'],
        hovertemplate='Node %{customdata}\'s total value is shown next to me!'
    ),
    link = dict(
        source = [0, 0, 0, 1, 1, 1, 2, 2, 2],
        target = [3, 4, 5, 3, 4, 5, 3, 4, 5],
        value  = [1, 6, 5, 3, 6, 3, 5, 6, 1],
        label  = ['Link {}'.format(i) for i in range(1, 10)],
        hovertemplate = '%{label} from node %{source.customdata}<br />' +
                        'to node %{target.customdata}<br />' +
                        'has value %{value}.<extra></extra>'
    )
)])
fig.update_layout(
    width = 1200,
    height = 600,
    title_text = 'Test <b>Sankey Diagram</b>',
    title_x = 0.5, # center
    font = dict(size=20, color='white'),
    paper_bgcolor = 'black',
    updatemenus = [dict(
        type = 'buttons',
        direction = 'left',
        x = 0.5,
        xanchor = 'auto',
        y = -0.05,
        showactive = False,
        buttons = [
            dict(
                args = ['orientation', 'h'],
                label = 'Horizontal',
                method = 'restyle'
            ),
            dict(
                args = ['orientation', 'v'],
                label = 'Vertical',
                method = 'restyle'
            )
        ]
    )]
)

fig.show(config=dict(displaylogo=False))
# fig.write_html('sankey_test.html')
