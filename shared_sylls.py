# interlingual homophones

import pandas as pd

# input
hyb = pd.read_csv('hyb', sep='\t')
# print(hyb)
yyb = pd.read_csv('yyb', sep='\t')
# print(yyb)

ss = pd.merge(hyb, yyb, on='ipa', sort=True)
ss = ss[['pinyin', 'jyutping', 'ipa']]
# print(ss)

# example characters

hyzd = pd.read_csv('hyzd', sep='\t')
# print(hyzd)

ss = pd.merge(ss, hyzd.drop_duplicates('pinyin'), how='left', on='pinyin')
ss = ss.rename(columns={'char': 'char_hy'})

yyzd = pd.read_csv('yyzd', sep='\t')
# print(yyzd)

ss = pd.merge(ss, yyzd.drop_duplicates('jyutping'), how='left', on='jyutping')
ss = ss.rename(columns={'char': 'char_yy'})

ss = ss[[ 'ipa', 'pinyin', 'char_hy', 'jyutping', 'char_yy']]
print(ss)

# output
ss.to_csv('ss', sep='\t', index=False)
# ss.to_html('ss.html', na_rep='', index=False)
