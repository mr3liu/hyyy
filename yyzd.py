import pandas as pd

# get necessary data from input
yyzd = pd.read_csv('yyzd_orig', sep='\t', usecols=[0, 2], header=None)
# print(yyzd)
yyzd.columns = ['char', 'jyutping']
# print(yyzd)

# modify
# splitting rows with multiple pronunciations
yyzd.jyutping = yyzd.jyutping.str.split('/')
yyzd = yyzd.explode('jyutping')
# print(yyzd)

# sort
yyzd = yyzd.sort_values(by='jyutping') # not sorted by index

# drop duplicates
yyzd = yyzd.drop_duplicates()
print(yyzd)

# output
yyzd.to_csv('yyzd', sep='\t', index=False)
