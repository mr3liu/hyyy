import pandas as pd

# get necessary data from input
hyb = pd.read_csv('hyb_orig', sep='\t', usecols=[0, 12], header=None)
# print(hyb)
hyb.columns = ['pinyin', 'ipa']
# print(hyb)

# modify
# removing usually unpronounced rising part of tone three
hyb.ipa = hyb.apply(lambda x: x.ipa[:(-1 if int(x.pinyin[-1]) == 3 else None)], axis=1)
print(hyb)

# output
hyb.to_csv('hyb', sep='\t', index=False)
