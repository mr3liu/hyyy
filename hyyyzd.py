import pandas as pd

# input
hyzd = pd.read_csv('hyzd', sep='\t')
yyzd = pd.read_csv('yyzd', sep='\t')

# remove duplicates to avoid confusion over which hy instance matches which yy instance
hyzd = hyzd.drop_duplicates('char', keep=False)
yyzd = yyzd.drop_duplicates('char', keep=False)

hyyyzd = pd.merge(hyzd, yyzd, on='char')

print(hyyyzd)

# output
hyyyzd.to_csv('hyyyzd', sep='\t', index=False)
