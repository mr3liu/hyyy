# nasal finals sankey diagram between hy and yy (detailed)

import pandas as pd

# input
hyyyzd = pd.read_csv('hyyyzd', sep='\t')
tp = hyyyzd.pinyin.str.slice(stop=-1) # toneless pinyin
tj = hyyyzd.jyutping.str.slice(stop=-1) # toneless jyutping
relevant_rows = tp.str.endswith('n') | \
                tp.str.endswith('ng') | \
                tj.str.endswith('m') | \
                tj.str.endswith('n') | \
                (tj.str.endswith('ng') & (tj != 'ng'))

hyyyzd = hyyyzd[relevant_rows]
hyyyzd = hyyyzd.reset_index()
tp = hyyyzd.pinyin.str.slice(stop=-1)
tj = hyyyzd.jyutping.str.slice(stop=-1)

sankey_link_sources = []
sankey_link_targets = []

for i in range(len(hyyyzd)):
    if tp[i].endswith('n'):
        sankey_link_sources += [0]
    elif tp[i].endswith('ng'):
        sankey_link_sources += [1]
    else:
        sankey_link_sources += [2]
    if tj[i].endswith('m'):
        sankey_link_targets += [3]
    elif tj[i].endswith('n'):
        sankey_link_targets += [4]
    elif tj[i].endswith('ng'):
        sankey_link_targets += [5]
    else:
        sankey_link_targets += [6]

sankey_link_labels = ['{} {} {}'.format(hyyyzd.char[i], hyyyzd.pinyin[i], hyyyzd.jyutping[i]) for i in range(len(hyyyzd))]
sankey_link_values = [1] * len(hyyyzd)

# remove others
# i = 0
# while True:
#     try:
#         if sankey_link_sources[i] == 2 or sankey_link_targets[i] == 6:
#             del sankey_link_sources[i]
#             del sankey_link_targets[i]
#             del sankey_link_labels[i]
#             del sankey_link_values[i]
#         else:
#             i += 1
#     except IndexError:
#         break

import plotly.graph_objects as go

fig = go.Figure(data=[go.Sankey(
    orientation = 'v',
    valueformat = '.0f',
    node = dict(
        pad = 25,
        thickness = 25,
        line = dict(width=0),
        label = ['pinyin -n', 'pinyin -ng', 'po', 'jyutping -m', 'jyutping -n', 'jyutping -ng', 'jo'], # pinyin others, jyutping others
        color = ['Salmon', 'Salmon', 'Salmon', 'MediumAquamarine', 'MediumAquamarine', 'MediumAquamarine', 'MediumAquamarine']
    ),
    link = dict(
        source = sankey_link_sources,
        target = sankey_link_targets,
        value  = sankey_link_values,
        label = sankey_link_labels,
        hovertemplate = '%{label}<extra></extra>'
    )
)])
fig.update_layout(
    width = 10000,
    title_text = 'Nasal Finals',
    title_x = 0,
    font_size = 20
)

fig.show(config=dict(displaylogo=False))
