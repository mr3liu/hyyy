import pandas as pd

# get necessary data from input
hyzd = pd.read_csv('hyzd_orig', sep='\t', usecols=[0, 2], header=None)
# print(hyzd)
hyzd.columns = ['char', 'pinyin']
# print(hyzd)

# modify
# splitting rows with multiple pronunciations
hyzd.pinyin = hyzd.pinyin.str.split('/')
hyzd = hyzd.explode('pinyin')
# print(hyzd)

# sort
hyzd = hyzd.sort_values(by='pinyin') # not sorted by index

# drop duplicates
hyzd = hyzd.drop_duplicates()
print(hyzd)

# output
hyzd.to_csv('hyzd', sep='\t', index=False)
