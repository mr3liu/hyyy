import re
import pandas as pd

# get data from html
hyb_l = []
with open('hyb_raw.html', encoding='utf-8') as f:
    for l in f:
        if re.match('\s*<td class="hov"', l):
            m = re.search('<span class="pin">(.+)</span><span class="ipa">(.+)</span>', l)
            if m:
                hyb_l.append((m.group(1), m.group(2)))
# print(hyb_l)

# format data as desired
hyb_1 = pd.DataFrame(hyb_l, columns=('pinyin', 'ipa'))
hyb_2 = hyb_1.copy()
hyb = pd.DataFrame()
for tone_p, tone_i in (('1', '˥˥'), ('2', '˧˥'), ('3', '˨˩'), ('4', '˥˩'), ('5', '˧')):
    hyb_2.pinyin = hyb_1.pinyin + tone_p
    hyb_2.ipa = hyb_1.ipa + tone_i
    hyb = pd.concat((hyb, hyb_2))

# sort
hyb = hyb.sort_values(by='pinyin') # not sorted by index
print(hyb)

# output
hyb.to_csv('hyb', sep='\t', index=False)
