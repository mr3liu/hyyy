# nasal finals sankey diagram between hy and yy

import pandas as pd

# input
hyyyzd = pd.read_csv('hyyyzd', sep='\t')
tp = hyyyzd.pinyin.str.slice(stop=-1) # toneless pinyin
tj = hyyyzd.jyutping.str.slice(stop=-1) # toneless jyutping
relevant_rows = tp.str.endswith('n') | \
                tp.str.endswith('ng') | \
                tj.str.endswith('m') | \
                tj.str.endswith('n') | \
                (tj.str.endswith('ng') & (tj != 'ng'))

hyyyzd = hyyyzd[relevant_rows]
tp = hyyyzd.pinyin.str.slice(stop=-1)
tj = hyyyzd.jyutping.str.slice(stop=-1)

sankey_link_values = [x.sum() for x in [
    tp.str.endswith('n') & tj.str.endswith('m'),
    tp.str.endswith('n') & tj.str.endswith('n'),
    tp.str.endswith('n') & tj.str.endswith('ng'),
    tp.str.endswith('ng') & tj.str.endswith('m'),
    tp.str.endswith('ng') & tj.str.endswith('n'),
    tp.str.endswith('ng') & tj.str.endswith('ng')
]]

import plotly.graph_objects as go

fig = go.Figure(data=[go.Sankey(
    orientation = 'v',
    valueformat = '.0f',
    node = dict(
        pad = 25,
        thickness = 25,
        line = dict(width=0),
        label = ['pinyin -n', 'pinyin -ng', 'jyutping -m', 'jyutping -n', 'jyutping -ng'],
        color = ['Salmon', 'Salmon', 'MediumAquamarine', 'MediumAquamarine', 'MediumAquamarine']
    ),
    link = dict(
        source = [0, 0, 0, 1, 1, 1],
        target = [2, 3, 4, 2, 3, 4],
        value  = sankey_link_values
    )
)])
fig.update_layout(
    title_text = 'Nasal Finals',
    title_x = 0.5,
    font_size = 20
)

fig.show(config=dict(displaylogo=False))
